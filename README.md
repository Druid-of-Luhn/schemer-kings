# Schemer Kings - A Scheme Grand Strategy Game About Scheming

_Schemer Kings_ is a text-based grand strategy game, written in Scheme for the Lisp Game Jam 2017 (easy mode). The player takes the role of the leader of a great family, scheming their way to political, economic and military domination of the continent. Continue the heritage of your dynasty, manage your territories and build your empire, crushing or assimilating your enemies in the process.

## Building and Running

The 'compile.scm' script uses the Chicken Scheme `system` egg to build the project. Build, load and execute in `csi` with one of the following:

- `make`
- `make run`
- `csi -q compile.scm`

Required Chicken Scheme eggs/srfis: `ansi-escape-sequences`, `data-structures`, `ioctl`, `srfi-1`, `srfi-13`, `srfi-27`, `srfi-69`, `srfi-133`, `system`.

Build files (`src/*.so` and `*.import.*`) can be removed with `make clean`.

If `rlwrap` is available on the system. `make [run]` will use it.

## Progress

Currently, only world generation and inspection is available. Play around with it, delve into the horror that is the code, and be impressed.

## License

Schemer Kings is licensed under GNU GPL-v3. See the LICENSE for more details.

Copyright (C) 2017 Billy Brown
