;; schemer-kings
;; Copyright (C) 2017  Billy Brown
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use system)

(define-system schemer-kings
    (scheme-file "src/voronoi")

    (scheme-file "src/utils")

    (scheme-file "src/world"
        includes: '("src/utils" "src/voronoi"))

    (scheme-file "src/render")

    (scheme-file "src/help")

    (scheme-file "src/handlers")

    (scheme-file "src/game"
        includes: '("src/handlers" "src/render" "src/world"))

    (compiled-scheme-file "src/schemer-kings"
        depends: '("src/game")))

(build-system schemer-kings)
(load-system schemer-kings)
(quit)
