RLWRAP = $(shell { command -v rlwrap || ""; } 2>/dev/null)

all: compile

run: compile

.PHONY: clean compile

clean:
	$(RM) $(PWD)/src/*.so *import*

compile:
	$(RLWRAP) csi -q $(PWD)/compile.scm
