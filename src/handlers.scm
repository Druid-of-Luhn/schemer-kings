;; schemer-kings
;; Copyright (C) 2017  Billy Brown
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(module sk.handlers (set-handlers get-handler call-handler ignore-param)
  (import chicken scheme)
  (use srfi-69)

  (define *handlers* #f)

  ;; Create the command->handler data structures if it does not
  ;; exist already, otherwise it does nothing.
  ;; @param handlers Association list of commands (strings) to handlers
  (define (set-handlers handlers)
    (cond
      ((not *handlers*)
        (set! *handlers*
          (alist->hash-table
            handlers
            string=?
            string-hash)))
      (else
        #f)))

  ;; Get the handler function for the given message
  ;; @param message The message for which to fetch the handler.
  ;; @return Function accepting a game object, or #f if the message is unrecognised.
  (define (get-handler message)
    (hash-table-ref/default
      *handlers*
      message
      #f))

  (define (call-handler handler params game)
    (((car handler)
      params)
     game))

  ;; Ignore the parameters passed to a handler.
  ;; @param handler Handler to ignore the parameters.
  ;; @return Handler function that accepts a game.
  (define (ignore-param handler)
    (lambda (_)
      handler)))
