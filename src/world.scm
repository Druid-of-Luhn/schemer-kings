;; schemer-kings
;; Copyright (C) 2017  Billy Brown
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(module sk.world (make-world get-territory)
  (import chicken scheme sk.utils sk.voronoi)
  (use data-structures srfi-1 srfi-27)

  ;; Modifier applied based on territory size
  (define (*territory-size-mod* size)
    (/ (sqrt size) 70))
  ;; Base minimum population number of a town
  (define *town-base-min-size* 400)
  ;; Base maximum population number of a town
  (define *town-base-max-size* 4000)

  ;; Create a world, with territories containing towns,
  ;; and borders between them.
  ;; Fields: 'size 'territories
  ;; @param size The number of territories in the world.
  (define (make-world size)
    (let ((territories (make-territories size)))
      (lambda (m)
        (case m
          ('size size)
          ('territories territories)))))

  ;; Create a given number of territories.
  ;; @param size The number of territories to make.
  ;; @return List of randomly generated territories.
  (define (make-territories size)
    (let* ((world-size (* 2 (* size size)))
           (capitals (make-voronoi (cons world-size world-size)
                                   (make-capitals size world-size)))
           (territories (map (lambda (c)
                               (territory-from-voronoi capitals c))
                             capitals))
           (borders (unique-borders
                      (fold (lambda (t bs)
                              (append bs (t 'borders)))
                            '()
                            territories))))
      (assign-borders borders territories)))

  ;; Correctly assign borders to territories.
  ;; @param borders List of all borders.
  ;; @param territories List of all territories.
  ;; @return Territories with borders correctly applied.
  (define (assign-borders borders territories)
    (map (lambda (t)
           (make-territory
             (t 'name)
             (t 'size)
             (t 'capital)
             (rename-borders
               t
               (filter (lambda (b)
                         (or (string=? ((t 'capital) 'name)
                                       ((car (b 'territories)) 'name))
                             (string=? ((t 'capital) 'name)
                                       ((cdr (b 'territories)) 'name))))
                       borders)
               territories)))
         territories))

  ;; Rename a territory's borders to reference territories instead of capitals.
  ;; @param territory The territory to have its borders renamed.
  ;; @param territories All territories in the world.
  ;; @return List of renamed borders.
  (define (rename-borders territory borders territories)
    (filter (lambda (x) x)
            (map (lambda (b)
                   (let ((between (b 'territories))
                         (attrs (b 'attributes)))
                     (cond
                       ((string=? ((territory 'capital) 'name)
                                  ((car between) 'name))
                        (make-border
                          (cons
                            territory
                            (find-by-capital (cdr between)
                                             territories))
                          attrs))
                       ((string=? ((territory 'capital) 'name)
                                  ((cdr between) 'name))
                        (make-border
                          (cons
                            territory
                            (find-by-capital (car between)
                                             territories))
                          attrs))
                       (else
                         #f))))
                 borders)))

  ;; Find a territory by its capital.
  ;; @param capital Capital city of the territory.
  ;; @param territories List of territories to search in.
  ;; @return Territory with the given capital as its capital city.
  (define (find-by-capital capital territories)
    (let ((ts (filter (lambda (t)
                        (equal? capital (t 'capital)))
                      territories)))
      (if (null? ts)
        capital
        (car ts))))

  ;; Make a list of capitals, placed in the world, to build the territories.
  ;; @param count Number of capitals to generate.
  ;; @param world-size Square world's dimensions.
  ;; @return List of capitals, placed in the world.
  (define (make-capitals count world-size)
    (list-count capitals to count
                (let ((x (+ 1 (random-integer world-size)))
                      (y (+ 1 (random-integer world-size))))
                  (cons
                    (cons
                      (cons x y)
                      (make-town
                        (random-name "capital" count)
                        (random-town-size
                          (+ 1 (random-integer 3)))))
                    capitals))))

  ;; Create a territory from a Voronoi cell.
  ;; @param cell The Voronoi cell to make the territory from.
  ;; @return A territory.
  (define (territory-from-voronoi cells cell)
    (let ((capital (cdar cell))
          (points (cdr cell)))
      (make-territory
        (random-name "territory" (length cells))
        (length points)
        capital
        (borders-from-voronoi cells cell))))

  ;; Create a territory node.
  ;; Fields: 'name 'size 'capital
  ;; @param name The territory's name.
  ;; @param size The size of the territory in km^2.
  ;; @param capital The territory's capital city.
  (define (make-territory name size capital borders)
    (lambda (m)
      (case m
        ('name name)
        ('size size)
        ('capital capital)
        ('borders borders))))

  ;; Get a territory by name.
  ;; @param world World from which to fetch the territory.
  ;; @param name Name of the territory to fetch.
  ;; @return Territory with the given name, or #f if it was not found.
  (define (get-territory world name)
    (let ((ts (filter (lambda (t)
                        (string=? (t 'name) name))
                      (world 'territories))))
      (cond
        ((null? ts)
         #f)
        (else
          (car ts)))))

  ;; Randomly pick a town population size.
  ;; @param Territory size modifier.
  ;; @return Population size of the town.
  (define (random-town-size territory-mod)
    (rand-min-max (* *town-base-min-size* territory-mod)
                  (* *town-base-max-size* territory-mod)))

  ;; Create a town.
  ;; Fields: 'name 'size
  ;; @param name The town's name.
  ;; @param size The town's population size.
  (define (make-town name size)
    (lambda (m)
      (case m
        ('name name)
        ('size size))))

  ;; Create borders between adjacent Voronoi cells.
  ;; @param cells List of all cells.
  ;; @param cell Cell being checked for borders.
  ;; @return List of borders between cell and others.
  (define (borders-from-voronoi cells cell)
    (let ((adjacent-cells 
            (list-unique
              (flatten
                (map (lambda (p)
                       (adjacent-voronoi
                         cells
                         (cdar cell)
                         p))
                     (cdr cell))))))
      (map (lambda (c)
             (make-border
               (cons (cdar cell) c)
               (border-attribute)))
           adjacent-cells)))

  ;; Generate a random name within a given range.
  ;; @param name Base name to apply.
  ;; @param range Range for random numbers.
  ;; @return Probably unique name.
  (define (random-name name range)
    (string-append
      name
      "-"
      (->string
        (random-integer
          (* 100 range)))))

  ;; Find the Voronoi cells adjacent to a point in a cell.
  ;; @param cells List of all Voronoi cells.
  ;; @param this-cell Cell being checked (without points).
  ;; @param point Point in the cell being checked.
  ;; @return List of Voronoi cells adjacent to the current one
  (define (adjacent-voronoi cells this-cell point)
    ; Only keep populated lists
    (filter (lambda (z)
              (not (null? z)))
            ; Remove invalid cells, or this-cell
            (map (lambda (x)
                   (filter (lambda (y)
                             (and y (not (equal? y this-cell))))
                           x))
                 ; For every point adjacent to this one
                 (map (lambda (p)
                        ; Find which cell it belongs to
                        (map (lambda (c)
                               (if (member p (cdr c))
                                 (cdar c)
                                 #f))
                             cells))
                      (adjacent-points point)))))

  ;; Get the adjacent points to a given point.
  ;; @param point Pair of coordinates of the point
  ;; @return List of points adjacent to the given point.
  (define (adjacent-points point)
    (list
      (cons (+ 1 (car point))      (cdr point))   ; right
      (cons (+ 1 (car point)) (+ 1 (cdr point)))  ; bottom-right
      (cons      (car point)  (+ 1 (cdr point)))  ; bottom
      (cons (- 1 (car point)) (+ 1 (cdr point)))  ; bottom-left
      (cons (- 1 (car point))      (cdr point))   ; left
      (cons (- 1 (car point)) (- 1 (cdr point)))  ; top-left
      (cons      (car point)  (- 1 (cdr point)))  ; top
      (cons (+ 1 (car point)) (- 1 (cdr point))))); top-right

  ;; Create a border between territories.
  ;; Fields: 'territories 'attributes
  ;; @param territories The pair of territories which share this border.
  ;; @param attributes A list of attributes for this border.
  (define (make-border territories attributes)
    (lambda (m)
      (case m
        ('territories territories)
        ('attributes attributes))))

  ;; Remove duplicate borders, compared by territory names.
  ;; @param borders List of all borders in the world.
  ;; @return List of borders with duplicates removed.
  (define (unique-borders borders)
    (cond
      ((null? borders)
       '())
      ((let loop ((border (car borders))
                  (bs (cdr borders)))
         (if (null? bs)
           #f
           (let ((between (border 'territories))
                 (bbtwn ((car bs) 'territories)))
             (if (or (and (string=? ((car between) 'name)
                                    ((car bbtwn) 'name))
                          (string=? ((cdr between) 'name)
                                    ((cdr bbtwn) 'name)))
                     (and (string=? ((car between) 'name)
                                    ((cdr bbtwn) 'name))
                          (string=? ((cdr between) 'name)
                                    ((car bbtwn) 'name))))
               #t
               (loop border (cdr bs))))))
       (unique-borders (cdr borders)))
      (else
        (cons (car borders)
              (unique-borders (cdr borders))))))
  
  ;; Pick a random border attribute.
  ;; @return Randomly chosen border attribute.
  (define (border-attribute)
    (let ((attributes '#("contested"
                         "a river"
                         "mountainous"
                         "hilly"
                         "a cliff")))
      (vector-ref attributes
                  (random-integer
                    (vector-length
                      attributes))))))
