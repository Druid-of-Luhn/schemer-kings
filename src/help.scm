;; schemer-kings
;; Copyright (C) 2017  Billy Brown
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(module sk.help (help-handler)
  (import chicken scheme)

  (define *help-lines*
    '("\
:h :help          display this screen, or get help for a
                  specific command with ':help <command>'
:q :quit          close the game
:territories      list all territories in the world
:territory <name> get information about the territory
                  with the name <name>"))

  (define (help-handler params)
    (lambda (game)
      (case params
        ('()
         (for-each (lambda (p)
                     (display p)
                     (newline))
                   *help-lines*))
        (else
          (display (string-append "help for " (car params)))
          (newline)))
      game)))
