;; schemer-kings
;; Copyright (C) 2017  Billy Brown
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(module sk.utils (rand-min-max list-unique list-count)
  (import chicken scheme)
  (use srfi-27)
  
  ;; Calculate a random integer between the given bounds.
  ;; @param min-value Lower bound.
  ;; @param max-value Upper bound.
  ;; @return Random integer in [min-value ; max-value[.
  (define (rand-min-max min-value max-value)
    (inexact->exact
      (floor
        (+ min-value
           (random-integer
             (inexact->exact
               (floor
                 (- max-value min-value))))))))

  ;; Remove all duplicate items in a list to make its elements unique.
  ;; @param lat Flat list to with elements to be made unique.
  ;; @return List with duplicate elements removed.
  (define (list-unique lat)
    (cond
      ((null? lat)
       '())
      ((member (car lat) (cdr lat))
       (list-unique (cdr lat)))
      (else
        (cons (car lat)
              (list-unique (cdr lat))))))

  ;; Generate a list of n elements, from a given code block.
  ;; Example: (list-count letters to 5
  ;;            (cons 'a letters))
  ;;          >>> (a a a)
  (define-syntax list-count
    (syntax-rules (to)
      ((list-count elems to count
                   body)
       (let loop ((counter count)
                  (elems '()))
         (cond
           ((<= counter 0)
            elems)
           (else
             (loop (- counter 1)
                   body))))))))
