;; schemer-kings
;; Copyright (C) 2017  Billy Brown
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(module sk.render (render-clear
                   render-prompt
                   render-territories
                   render-territory)
  (import chicken scheme)
  (use ansi-escape-sequences extras ioctl)

  (define *prompt* (set-text '(fg-yellow) "sk> "))

  (define (render-clear)
    (let loop ((lines (car (ioctl-winsize))))
      (case lines
        ((0))
        (else
          (newline)
          (loop (- lines 1))))))

  (define (render-prompt)
    (display *prompt*))

  (define (render-territories game)
    (format #t "~a~%"
            (map (lambda (t)
                   (format #f "[~a ~a km² ~a]"
                           (emph (t 'name))
                           (t 'size)
                           ((t 'capital) 'name)))
                 ((game 'world) 'territories)))
    game)

  (define (render-territory territory)
    (format #t "~a~a~%"
            (label "   Name ")
            (emph
              (territory 'name)))
    (format #t "~a~a km²~%"
            (label "   Size ")
            (territory 'size))
    (format #t "~a~a~%"
            (label "Capital ")
            (render-town (territory 'capital)))
    (format #t "~a~a~%"
            (label "Borders ")
            (map render-border (territory 'borders))))

  (define (render-town town)
    (format #f "~a (pop ~a)" (emph (town 'name)) (town 'size)))

  (define (render-border border)
    (format #f "~%      • The border with ~a is ~a."
            (emph ((cdr (border 'territories)) 'name))
            (border 'attributes)))

  (define (label str)
    (set-text '(fg-green) str))
  
  (define (emph str)
    (set-text '(underscore) str)))
