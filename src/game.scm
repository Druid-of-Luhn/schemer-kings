;; schemer-kings
;; Copyright (C) 2017  Billy Brown
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(module sk.game (game-start)
  (import chicken scheme sk.handlers sk.help sk.render sk.world)
  (use extras srfi-1 srfi-13)

  (define (game-start)
    (render-clear)
    (make-handlers)
    (game-loop
      (make-game 5)
      null?)
    #t)
  
  (define (game-loop game end?)
    (cond
      ((end? game))
      (else
        (game-loop (update game) end?))))

  (define (update game)
    (render-prompt)
    (let ((line (string-tokenize
                  (read-line))))
      (cond
        ((null? line)
         game)
        (else
          (let ((handler (get-handler (car line)))
                (params (cdr line)))
            (cond
              ((not handler)
                (display "enter :help for a list of available commands")
                (newline)
                game)
              (else
               (call-handler handler params game))))))))

  (define (make-game world-size)
    (let ((world (make-world world-size)))
      (lambda (m)
        (case m
          ('world world)))))

  (define (quit-game _)
    '())

  (define (make-handlers)
    (set-handlers
      `(
        (":help" ,help-handler)
        (":h" ,help-handler)
        (":quit" ,(ignore-param quit-game))
        (":q" ,(ignore-param quit-game))
        (":territories" ,(ignore-param render-territories))
        (":territory" ,(lambda (params)
                         (lambda (game)
                           (cond
                             ((null? params)
                              game)
                             (else
                               (render-territory
                                 (get-territory
                                   (game 'world)
                                   (car params)))
                               game)))))
        ))))
