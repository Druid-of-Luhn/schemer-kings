;; schemer-kings
;; Copyright (C) 2017  Billy Brown
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(module sk.voronoi (make-voronoi)
  (import chicken scheme)
  (use srfi-1 srfi-133)

  ;; Calculate the Voronoi cells in a space of given dimensions
  ;; and hub points.
  ;; @param dimensions Pair of width and height of the space.
  ;; @param points List of hub points to build the cells from.
  ;; @return Pair of hub points, and a list of all points in the
  ;; space, with the hub point they belong to.
  (define (make-voronoi dimensions points)
    (let ((width (car dimensions))
          (height (cdr dimensions))
          (new-point (point-maker dimensions)))
      (vector-fold merge-points
                   (map list points)
                   (vector-map (lambda (_)
                                 (voronoi-belonging
                                   points
                                   (new-point)))
                               (make-vector (* width height))))))

  ;; Create a function that produces points within the given
  ;; dimensions across calls.
  ;; @param dimensions Pair of width and height of the space.
  ;; @return Function with no arguments that produces the next
  ;; point in the sequence, from top-to-bottom and left-to-right.
  (define (point-maker dimensions)
    (let ((width (car dimensions))
          (height (cdr dimensions))
          (point (cons 1 1)))
      (lambda ()
        (let ((p point))
          (cond
            ;; End of the row
            ((>= (car p) width)
             ;; End of the space, start again
             (if (>= (cdr p) height)
               (set! point
                 (cons 1 1))
               ;; Move down to the start of the next row
               (set! point
                 (cons 1 (+ 1 (cdr p))))))
            ;; Move along the row
            (else
              (set! point
                (cons (+ 1 (car p)) (cdr p)))))
          p))))

  ;; Determine which hub point the given point 'belongs' to.
  ;; @param points List of hub points.
  ;; @param p Point being checked.
  ;; @return Pair of the point and the hub point's metadata.
  (define (voronoi-belonging points p)
    (let* ((distances (map (lambda (point)
                             (distance
                               (car point)
                               p))
                           points))
           (closest (fold (lambda (point best)
                            (cond
                              ((not best)
                               point)
                              ((< (car point)
                                  (car best))
                               point)
                              (else
                                best)))
                          #f
                          (map cons distances points))))
      (cons p (cdr closest))))

  ;; Insert points into lists on the hub points.
  ;; @param points The hub points.
  ;; @param point The point to insert.
  ;; @return The hub points, with individual points added.
  (define (merge-points points point)
    (map (lambda (p)
           (cond
             ((equal? (cdr point) (car p))
              (cons (car p)
                    (cons (car point) (cdr p))))
             (else
               p)))
         points))

  ;; Calculate the Manhattan distance between two points.
  ;; @param p1 Pair of coordinates (x . y).
  ;; @param p2 Pair of coordinates (x . y).
  ;; @return Manhattan istance between the two points.
  (define (distance p1 p2)
    (+
      (abs (- (car p1)
              (car p2)))
      (abs (- (cdr p1)
              (cdr p2))))))
